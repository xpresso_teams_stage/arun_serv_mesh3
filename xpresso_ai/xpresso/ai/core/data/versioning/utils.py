from datetime import datetime as date_time, timedelta
from re import match as regex_match
from os import path as path_library, makedirs as make_dirs_library, \
    walk as walk_dir_library

from xpresso.ai.core.commons.exceptions.xpr_exceptions \
    import PachydermFieldsNameException, LocalFilePathException

# regex for only alphanumeric characters & underscore
ACCEPTED_FIELD_NAME_REGEX = r"[\w, -]+$"
OUTPUT_DATE_STR_FORMAT = "%d/%m/%y"


def find_date_from_seconds(number_of_seconds):
    """
    Takes number of seconds as input and outputs date from 1970/1/1

    :param number_of_seconds:
        number of seconds
    :return:
        date as string
    """
    start_date = date_time(1970, 1, 1)
    new_date_object = start_date + timedelta(seconds=number_of_seconds)
    new_date = new_date_object.strftime(OUTPUT_DATE_STR_FORMAT)
    return new_date


def name_validity_check(field, value):
    """
    Checks if the name provided contains only alphanumeric characters,
    underscore or dashes

    :param field:
        field key
    :param value:
        value of the field key
    :return:
    """
    if not isinstance(value, str):
        raise PachydermFieldsNameException()
    match = regex_match(ACCEPTED_FIELD_NAME_REGEX, value)
    if not match:
        raise PachydermFieldsNameException(
            f"Invalid '{field}' field value: {value}\nOnly alphanumeric "
            f"characters and underscores are allowed."
        )


def write_to_file(path, content):
    """
    writes the contents of automl to a local file

    :param path:
        local path of the file
    :param content:
        file content
    :return:
    """
    if path_library.isfile(path_library.dirname(path)):
        # TODO: Remove files that are added already to local system
        raise LocalFilePathException(f"file exists at this path; {path}")

    file_extension = path_library.splitext(path)[1]
    if not path_library.exists(path) and \
            not path_library.isdir(path_library.dirname(path)):
        make_dirs_library(path_library.dirname(path))

    with open(path, "wb") as out_file:
        out_file.write(content)

    return


def get_pickle_from_dir(base_path):
    """
    fetches the path to pickle file inside the base_path dir

    :param base_path:
        path to the base directory
    :return:
        path to the pickle file
    """
    for current_dir_path, subdir_list, files in walk_dir_library(base_path):
        for file in files:
            file_path = path_library.join(current_dir_path, file)
            file_extension = path_library.splitext(file_path)[1]
            if file_extension == ".pkl":
                return file_path
    return None


def get_json_from_dir(base_path):
    """
    fetches the path to json file inside the base_path dir

    :param base_path:
        path to the base directory
    :return:
        path to the json file
    """
    for current_dir_path, subdir_list, files in walk_dir_library(base_path):
        for file in files:
            file_path = path_library.join(current_dir_path, file)
            file_extension = path_library.splitext(file_path)[1]
            if file_extension == ".json":
                return file_path
    return None


def create_pachyderm_path(local_file_path, input_dataset_path,
                          dir_path_on_pachyderm):
    """
    takes the path of file on local system and generates new path for it
    on pachyderm cluster

    :param local_file_path:
        file path on the local system
    :param input_dataset_path:
        path of the input dataset provided
    :param dir_path_on_pachyderm:
        path of directory on pachyderm cluster inside which new file
        should be added
    :return:
    """
    rel_file_path = path_library.relpath(local_file_path, input_dataset_path)
    pachyderm_local_path = path_library.join(dir_path_on_pachyderm,
                                             rel_file_path)
    return pachyderm_local_path


def fetch_file_path_list(dataset_dir, dataset_name):
    """
    fetches the list of file paths recursively in a directory

    :param dataset_dir:
        path of the dataset directory
    :param dataset_name:
        name of the dataset
    :return:
        returns a list of file paths inside the dataset directory
    """
    file_list = []
    pachyderm_destination_path = f"dataset/{dataset_name}"

    for dir_path, dirs, files in walk_dir_library(dataset_dir):
        for file in files:
            file_path = path_library.join(dir_path, file)
            destination_path = create_pachyderm_path(
                file_path, dataset_dir, pachyderm_destination_path)
            file_list.append((file_path, destination_path))

    return file_list
